﻿using Backend.Domain.Core.Repositories;

namespace Backend.Infra.Repositories;

public class UnitOfWork : IUnitOfWork
{
    private readonly BackendContext _context;

    public UnitOfWork(BackendContext context)
    {
        _context = context;
    }

    public async Task<bool> CommitAsync(CancellationToken cancellationToken)
    {
        return (await _context.SaveChangesAsync(cancellationToken)) > 0;
    }
}