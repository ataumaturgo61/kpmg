﻿using Azure;
using Backend.Domain.Entities;
using Backend.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Backend.Infra.Repositories;

public class PersonRepository : IPersonRepository
{
    private readonly BackendContext _context;

    public PersonRepository(BackendContext context)
    {
        _context = context;
    }

    public async Task<bool> CpfExists(string cpf)
    {
        return await _context.Persons.AnyAsync(x => x.Cpf.Value == cpf);
    }

    public async Task Add(Person person)
    {
        await _context.AddAsync(person);
    }

    public void Delete(Person person)
    {
        _context.Remove(person);
    }

    public void Update(Person person)
    {
        _context.Persons.Update(person);
    }

    public async Task<Person> FindById(Guid id)
    {
        var response = await _context.Persons.Include(x => x.Cars).FirstOrDefaultAsync(x => x.Id == id);

        return response;
    }

    public async Task<IEnumerable<Person>> GetAll()
    {
        return await _context.Persons.Include(x => x.Cars).ToListAsync();
    }

    public async Task AddCars(IEnumerable<Car> cars)
    {
        await _context.Cars.AddRangeAsync(cars);
    }

    public async Task AddCar(Car car)
    {
        await _context.Cars.AddAsync(car);
    }

    public void RemoveCar(Car car)
    {
        _context.Remove(car);
    }

    public void RemoveCars(IEnumerable<Car> cars)
    {
        _context.RemoveRange(cars);
    }

    public async Task<Person> GetById(Guid personId)
    {
        return await _context.Persons.FirstOrDefaultAsync(x => x.Id == personId);
    }

    public async Task<IEnumerable<Car>> GetCars(List<Guid> carsIds)
    {
        return await _context.Cars.Where(x => carsIds.Contains(x.Id)).ToListAsync();
    }

    public async Task<IEnumerable<Car>> GetCars(Guid personId)
    {
        var response = await _context.Cars.Where(x => x.PersonId == personId).ToListAsync();

        return response;
    }
}