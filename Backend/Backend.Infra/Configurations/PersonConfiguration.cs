﻿using Backend.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Backend.Infra.Configurations;

public class PersonConfiguration : IEntityTypeConfiguration<Person>
{
    public void Configure(EntityTypeBuilder<Person> builder)
    {
        builder.HasKey(p => p.Id);

        builder.OwnsOne(p => p.Cpf, x =>
        {
            x.Property(x => x.Value)
                .HasColumnName("Cpf")
                .HasMaxLength(11);

            x.HasIndex(x => x.Value).HasName("CpfIndex");
        });

        builder.Property(p => p.Name)
            .IsRequired();

        builder.HasMany(p => p.Cars)
            .WithOne(c => c.Person)
            .HasForeignKey(c => c.PersonId); 
    }
}