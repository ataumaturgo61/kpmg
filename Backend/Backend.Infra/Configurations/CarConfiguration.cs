﻿using Backend.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Backend.Infra.Configurations;

public class CarConfiguration : IEntityTypeConfiguration<Car>
{
    public void Configure(EntityTypeBuilder<Car> builder)
    {
        builder.HasKey(c => c.Id);

        builder.OwnsOne(c => c.Renavam, x =>
        {
            x.Property(x => x.Value)
                .HasColumnName("Renavam");
        });

        builder.Property(c => c.LicensePlate)
            .IsRequired()
            .HasMaxLength(20);

        builder.HasOne(c => c.Person)
            .WithMany(p => p.Cars)
            .HasForeignKey(c => c.PersonId);
    }
}