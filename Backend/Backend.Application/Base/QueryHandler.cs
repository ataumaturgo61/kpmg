﻿using AutoMapper;
using MediatR;

namespace Backend.Application.Base;

public abstract class QueryHandler<TQuery, TResponse> : IRequestHandler<TQuery, TResponse>
    where TQuery : Query<TResponse> where TResponse : class
{
    protected readonly IMapper _mapper; 
    protected QueryHandler(IMapper mapper)
    {
        _mapper = mapper;
    }

    public abstract Task<TResponse> Handle(TQuery request, CancellationToken cancellationToken);
}