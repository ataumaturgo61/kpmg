﻿using Backend.Domain.Core.Repositories;
using FluentValidation.Results;
using MediatR;

namespace Backend.Application.Base;

public abstract class CommandHandler<T> : IRequestHandler<T, ValidationResult> where T : Command
{ 
    protected readonly IUnitOfWork _unitOfWork;

    protected CommandHandler(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    public abstract Task<ValidationResult> Handle(T request, CancellationToken cancellationToken); 
}