﻿using FluentValidation.Results;

namespace Backend.Application.Base;

public static class CommandExtensions
{
    public static ValidationResult Success(this Command command)
    {
        command.ValidationResult.Errors.Clear();

        return command.ValidationResult;
    }

    public static ValidationResult Fail(this Command command)
    {  
        return command.ValidationResult;
    }

    public static ValidationResult Fail(this Command command, string errorMessage)
    {
        command.ValidationResult.Errors.Add(new ValidationFailure
        {
            ErrorMessage = errorMessage
        });

        return command.ValidationResult;
    }

    public static ValidationResult Fail(this Command command, IEnumerable<string> errorMessages)
    {
        var validationResults = errorMessages.Select(errorMessage => new ValidationFailure
        {
            ErrorMessage = errorMessage
        });

        command.ValidationResult.Errors.AddRange(validationResults);

        return command.ValidationResult;
    }
}