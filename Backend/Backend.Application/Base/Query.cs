﻿using MediatR;

namespace Backend.Application.Base;

public abstract class Query<TResponse> : IRequest<TResponse> where TResponse : class { }