﻿using MediatR;
using System.Text.Json.Serialization;
using ValidationResult = FluentValidation.Results.ValidationResult;

namespace Backend.Application.Base;

public abstract class Command : IRequest<ValidationResult>
{
    [JsonIgnore] public ValidationResult ValidationResult { get; set; } = new();
}