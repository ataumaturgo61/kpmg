﻿using FluentValidation;

namespace Backend.Application.Person.AddCar;

public class AddCarsCommandValidation : AbstractValidator<AddCarCommand>
{
    public AddCarsCommandValidation()
    {
        RuleFor(x => x.Renavam)
            .NotNull()  
            .NotEmpty();

    }
}