﻿using Backend.Application.Base;

namespace Backend.Application.Person.AddCar;

public class AddCarCommand : Command
{
    public string Renavam { get; set; }
    public string LicensePlate { get; set; }
    public Guid PersonId { get; set; }

    public bool IsValid()
    {
        ValidationResult = new AddCarsCommandValidation().Validate(this);
        return ValidationResult.IsValid;
    }
}