﻿using Backend.Application.Base;
using Backend.Domain.Core.Repositories;
using Backend.Domain.Entities;
using Backend.Domain.Repositories;
using FluentValidation.Results;

namespace Backend.Application.Person.AddCar;

public class AddCarsCommandHandler : CommandHandler<AddCarCommand>
{
    private readonly IPersonRepository _personRepository;
    public AddCarsCommandHandler(IUnitOfWork unitOfWork, IPersonRepository personRepository) : base(unitOfWork)
    {
        _personRepository = personRepository;
    }

    public override async Task<ValidationResult> Handle(AddCarCommand request, CancellationToken cancellationToken)
    { 
        if (!request.IsValid())
        {
            return request.Fail();
        }

        var person = await _personRepository.GetById(request.PersonId);
        if (person is null)
        {
            return request.Fail("Pessoa não cadastrada.");
        }

        var car = new Car(request.Renavam, request.LicensePlate, request.PersonId);

        await _personRepository.AddCar(car);

        await _unitOfWork.CommitAsync(cancellationToken);

        return request.Success();
    }
}