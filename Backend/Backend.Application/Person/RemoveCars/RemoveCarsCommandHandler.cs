﻿using Backend.Application.Base;
using Backend.Domain.Core.Repositories;
using Backend.Domain.Entities;
using Backend.Domain.Repositories;
using FluentValidation.Results;

namespace Backend.Application.Person.RemoveCars;

public class RemoveCarsCommandHandler : CommandHandler<RemoveCarsCommand>
{
    private readonly IPersonRepository _personRepository;
    public RemoveCarsCommandHandler(IUnitOfWork unitOfWork, IPersonRepository personRepository) : base(unitOfWork)
    {
        _personRepository = personRepository;
    }

    public override async Task<ValidationResult> Handle(RemoveCarsCommand request, CancellationToken cancellationToken)
    {
        if (!request.IsValid())
        {
            return request.Fail();
        }

        IEnumerable<Car> cars = 
            await _personRepository.GetCars(request.CarsIds);

        _personRepository.RemoveCars(cars); 
        await _unitOfWork.CommitAsync(cancellationToken);

        return request.Success();
    }
}