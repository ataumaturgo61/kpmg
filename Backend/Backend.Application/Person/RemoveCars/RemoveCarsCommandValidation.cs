﻿using FluentValidation;

namespace Backend.Application.Person.RemoveCars;

public class RemoveCarsCommandValidation : AbstractValidator<RemoveCarsCommand>
{
    public RemoveCarsCommandValidation()
    {
        RuleFor(x => x.CarsIds)
            .NotEmpty()
            .NotNull();
    }
}