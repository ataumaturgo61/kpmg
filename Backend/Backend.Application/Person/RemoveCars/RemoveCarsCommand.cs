﻿using Backend.Application.Base;
using Backend.Application.Person.CreatePerson;

namespace Backend.Application.Person.RemoveCars;

public class RemoveCarsCommand : Command
{
    public List<Guid> CarsIds { get; set; } = new();

    public bool IsValid()
    {
        ValidationResult = new RemoveCarsCommandValidation().Validate(this);
        return ValidationResult.IsValid;
    }
}