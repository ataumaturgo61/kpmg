﻿using Backend.Application.Base;

namespace Backend.Application.Person.RemovePerson;

public class RemovePersonCommand : Command
{
    public Guid PersonId { get; set; }
}