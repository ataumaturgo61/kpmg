﻿using FluentValidation;

namespace Backend.Application.Person.RemovePerson;

public class RemovePersonCommandValidation : AbstractValidator<RemovePersonCommand>
{
    public RemovePersonCommandValidation()
    {
        RuleFor(x => x.PersonId)
            .NotEmpty();
    }
}