﻿using Backend.Application.Base;
using Backend.Domain.Core.Repositories;
using Backend.Domain.Repositories;
using FluentValidation.Results;

namespace Backend.Application.Person.RemovePerson;

public class RemovePersonCommandHandler : CommandHandler<RemovePersonCommand>
{
    private readonly IPersonRepository _personRepository;

    public RemovePersonCommandHandler(IUnitOfWork unitOfWork, IPersonRepository personRepository) : base(unitOfWork)
    {
        _personRepository = personRepository;
    }

    public override async Task<ValidationResult> Handle(RemovePersonCommand request, CancellationToken cancellationToken)
    {
        var person = await _personRepository.FindById(request.PersonId);
        if (person == null)
        {
            return request.Fail("Pessoa não encontrada");
        }

        _personRepository.Delete(person);
        await _unitOfWork.CommitAsync(cancellationToken);

        return request.Success();
    }
}