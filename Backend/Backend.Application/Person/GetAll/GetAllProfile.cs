﻿using AutoMapper;

namespace Backend.Application.Person.GetAll;

public class GetAllProfile : Profile
{
    public GetAllProfile()
    {
        CreateMap<Domain.Entities.Person, GetAllQueryResponse>()
            .ForPath(x => x.Cpf, p => p.MapFrom(x => x.Cpf.Value));
    }
}