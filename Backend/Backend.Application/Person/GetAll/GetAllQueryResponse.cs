﻿namespace Backend.Application.Person.GetAll;

public record GetAllQueryResponse(Guid Id, string Name, string Cpf);