﻿using Backend.Application.Base;

namespace Backend.Application.Person.GetAll;

public class GetAllQuery : Query<IEnumerable<GetAllQueryResponse>> { }