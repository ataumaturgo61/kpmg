﻿using AutoMapper;
using Backend.Application.Base;
using Backend.Domain.Repositories;

namespace Backend.Application.Person.GetAll;

public class GetAllQueryHandler : QueryHandler<GetAllQuery, IEnumerable<GetAllQueryResponse>>
{
    private readonly IPersonRepository _personRepository;

    public GetAllQueryHandler(IMapper mapper, IPersonRepository personRepository) : base(mapper)
    {
        _personRepository = personRepository;
    }

    public override async Task<IEnumerable<GetAllQueryResponse>> Handle(GetAllQuery request, CancellationToken cancellationToken)
    {
        var personResponse =
            await _personRepository.GetAll();

        var response = _mapper.Map<List<GetAllQueryResponse>>(personResponse);

        return response;
    }


}