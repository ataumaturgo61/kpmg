﻿using FluentValidation;

namespace Backend.Application.Person.CreatePerson;

public class CreatePersonCommandValidation : AbstractValidator<CreatePersonCommand>
{
    public CreatePersonCommandValidation()
    {
        RuleFor(x => x.Cpf)
            .NotNull()
            .MinimumLength(11)
            .MaximumLength(11)
            .NotEmpty();  
        
        RuleFor(x => x.Name)
            .NotNull() 
            .NotEmpty();

    }
}