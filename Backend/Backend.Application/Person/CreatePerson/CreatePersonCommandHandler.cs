﻿using Backend.Application.Base;
using Backend.Domain.Core.Repositories;
using Backend.Domain.Repositories;
using Backend.Domain.Services;
using FluentValidation.Results;

namespace Backend.Application.Person.CreatePerson;

public class CreatePersonCommandHandler : CommandHandler<CreatePersonCommand>
{
    private readonly IPersonRepository _personRepository;
    private readonly IPersonService _personService;

    public CreatePersonCommandHandler(IUnitOfWork unitOfWork, IPersonRepository personRepository, IPersonService personService) : base(unitOfWork)
    {
        _personRepository = personRepository;
        _personService = personService;
    }

    public override async Task<ValidationResult> Handle(CreatePersonCommand request, CancellationToken cancellationToken)
    {
        if (!request.IsValid())
        {
            return request.Fail();
        }

        if (!_personService.IsValidCpf(request.Cpf))
        {
            return request.Fail("CPF inválido.");
        }

        var documentExists =
            await _personRepository.CpfExists(request.Cpf);

        if (documentExists)
        { 
            return request.Fail("CPF já existente.");
        }

        Domain.Entities.Person person = new(request.Name, request.Cpf);

        await _personRepository.Add(person); 
        await _unitOfWork.CommitAsync(cancellationToken);

        return request.Success();
    }

}