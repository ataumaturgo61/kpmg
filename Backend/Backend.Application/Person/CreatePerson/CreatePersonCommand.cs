﻿using Backend.Application.Base;

namespace Backend.Application.Person.CreatePerson;

public class CreatePersonCommand : Command
{
    public string Name { get; set; } = string.Empty;
    public string Cpf { get; set; } = string.Empty;

    public bool IsValid()
    {
        ValidationResult = new CreatePersonCommandValidation().Validate(this);
        return ValidationResult.IsValid;
    }
}