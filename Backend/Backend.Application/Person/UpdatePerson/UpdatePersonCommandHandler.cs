﻿using Backend.Application.Base;
using Backend.Domain.Core.Repositories;
using Backend.Domain.Repositories;
using Backend.Domain.Services;
using FluentValidation.Results;

namespace Backend.Application.Person.UpdatePerson;

public class UpdatePersonCommandHandler : CommandHandler<UpdatePersonCommand>
{
    private readonly IPersonRepository _personRepository;
    private readonly IPersonService _personService;
    public UpdatePersonCommandHandler(IUnitOfWork unitOfWork, IPersonRepository personRepository, IPersonService personService) : base(unitOfWork)
    {
        _personRepository = personRepository;
        _personService = personService;
    }

    public override async Task<ValidationResult> Handle(UpdatePersonCommand request, CancellationToken cancellationToken)
    {
        if (!request.IsValid())
        {
            return request.Fail();
        }

        var person = await _personRepository.FindById(request.PersonId);
        if (person == null)
        {
            return request.Fail("Pessoa não encontrada.");
        }

        if (!string.IsNullOrWhiteSpace(request.Name))
            person.ChangeName(request.Name);

        if (!string.IsNullOrWhiteSpace(request.Cpf))
        {
            if (!_personService.IsValidCpf(request.Cpf))
            {
                return request.Fail("CPF inválido");
            }

            person.ChangeCpf(request.Cpf);
        }

        _personRepository.Update(person);
        await _unitOfWork.CommitAsync(cancellationToken);

        return request.Success();
    }
}