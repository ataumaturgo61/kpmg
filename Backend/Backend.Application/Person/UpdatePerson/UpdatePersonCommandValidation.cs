﻿using FluentValidation;

namespace Backend.Application.Person.UpdatePerson;

public class UpdatePersonCommandValidation : AbstractValidator<UpdatePersonCommand>
{
    public UpdatePersonCommandValidation()
    {
        RuleFor(x => x.PersonId)
            .NotEmpty();

        RuleFor(x => x.Cpf)
            .MinimumLength(11)
            .MaximumLength(11);
    }
}