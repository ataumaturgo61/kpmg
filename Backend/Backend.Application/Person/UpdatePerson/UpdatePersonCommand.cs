﻿using Backend.Application.Base;
using Backend.Application.Person.RemoveCars;

namespace Backend.Application.Person.UpdatePerson;

public class UpdatePersonCommand : Command
{
    public Guid PersonId { get; set; }
    public string Name { get; set; }
    public string Cpf { get; set; }

    public bool IsValid()
    {
        ValidationResult = new UpdatePersonCommandValidation().Validate(this);
        return ValidationResult.IsValid;
    }
}