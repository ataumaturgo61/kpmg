﻿namespace Backend.Application.Person.GetById;

public class GetByIdQueryResponse
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Cpf { get; set; }
    public int Cars { get; set; }
}