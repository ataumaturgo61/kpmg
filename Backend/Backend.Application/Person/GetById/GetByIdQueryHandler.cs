﻿using AutoMapper;
using Backend.Application.Base;
using Backend.Domain.Repositories;

namespace Backend.Application.Person.GetById;

public class GetByIdQueryHandler : QueryHandler<GetByIdQuery, GetByIdQueryResponse>
{
    private readonly IPersonRepository _personRepository;
    public GetByIdQueryHandler(IMapper mapper, IPersonRepository personRepository) : base(mapper)
    {
        _personRepository = personRepository;
    }

    public override async Task<GetByIdQueryResponse> Handle(GetByIdQuery request, CancellationToken cancellationToken)
    {
        var person = await _personRepository.FindById(request.PersonId);

        var response = _mapper.Map<GetByIdQueryResponse>(person);

        return response;
    }
}