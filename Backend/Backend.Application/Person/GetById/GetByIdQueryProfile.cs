﻿using AutoMapper;

namespace Backend.Application.Person.GetById;

public class GetByIdQueryProfile : Profile
{
    public GetByIdQueryProfile()
    {
        CreateMap<Domain.Entities.Person, GetByIdQueryResponse>()
            .ForPath(x => x.Cpf, p => p.MapFrom(x => x.Cpf.Value))
            .ForPath(x => x.Cars, p => p.MapFrom(x => x.Cars.Count));

    }
}