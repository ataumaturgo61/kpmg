﻿using Backend.Application.Base;

namespace Backend.Application.Person.GetById;

public class GetByIdQuery : Query<GetByIdQueryResponse>
{
    public Guid PersonId { get; set; }

    public GetByIdQuery(Guid personId)
    {
        PersonId = personId;
    }
}