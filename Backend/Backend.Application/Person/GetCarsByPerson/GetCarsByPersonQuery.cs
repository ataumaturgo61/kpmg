﻿using Backend.Application.Base;

namespace Backend.Application.Person.GetCarsByPerson;

public class GetCarsByPersonQuery : Query<IEnumerable<GetCarsByPersonQueryResponse>>
{
    public Guid PersonId { get; set; }
}

public class GetCarsByPersonQueryResponse
{
    public string Renavam { get; set; }
    public string LicensePlate { get; set; }
}