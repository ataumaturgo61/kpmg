﻿using AutoMapper;
using Backend.Application.Base;
using Backend.Domain.Repositories;

namespace Backend.Application.Person.GetCarsByPerson;

public class GetCarsByPersonQueryHandler : QueryHandler<GetCarsByPersonQuery, IEnumerable<GetCarsByPersonQueryResponse>>
{
    private readonly IPersonRepository _personRepository;

    public GetCarsByPersonQueryHandler(IMapper mapper, IPersonRepository personRepository) : base(mapper)
    {
        _personRepository = personRepository;
    }

    public override async Task<IEnumerable<GetCarsByPersonQueryResponse>> Handle(GetCarsByPersonQuery request,
        CancellationToken cancellationToken)
    {
        var person = await _personRepository.FindById(request.PersonId);
        if (person == null)
        {
            return default;
        }

        var cars =
            await _personRepository.GetCars(request.PersonId);

        var response = _mapper.Map<List<GetCarsByPersonQueryResponse>>(cars);

        return response;
    }
}