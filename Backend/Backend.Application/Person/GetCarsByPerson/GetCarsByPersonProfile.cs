﻿using AutoMapper;
using Backend.Domain.Entities;

namespace Backend.Application.Person.GetCarsByPerson;

public class GetCarsByPersonProfile : Profile
{
    public GetCarsByPersonProfile()
    {
        CreateMap<Car, GetCarsByPersonQueryResponse>()
            .ForPath(x => x.Renavam, x => x.MapFrom(x => x.Renavam.Value));
    }
}