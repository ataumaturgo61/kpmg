﻿using System.Text;

namespace Backend.Domain.Core.Extensions;

public static class StringExtensions
{
    public static bool OnlyNumbers(this string value)
    {
        if (string.IsNullOrEmpty(value))
            return false;

        foreach (char c in value)
        {
            if (!char.IsDigit(c))
                return false;
        }

        return true;
    }

    public static string RemoveNonNumbers(this string value)
    {
        if (string.IsNullOrEmpty(value))
            return string.Empty;

        StringBuilder result = new StringBuilder();

        foreach (char c in value)
        {
            if (char.IsDigit(c))
            {
                result.Append(c);
            }
        }

        return result.ToString();
    }
}