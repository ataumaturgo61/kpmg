﻿using Backend.Domain.Core.Entities;

namespace Backend.Domain.Core.Repositories;

public interface IRepository<TAggregate> where TAggregate : IAggregateRoot { }