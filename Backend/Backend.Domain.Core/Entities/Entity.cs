﻿namespace Backend.Domain.Core.Entities;

public abstract class Entity
{
    public Guid Id { get; private set; }

    protected Entity()
    {
        Id = Guid.NewGuid();
    }
}

public interface IAggregateRoot { }

public abstract class ValueObject
{

}