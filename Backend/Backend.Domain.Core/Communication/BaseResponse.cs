﻿namespace Backend.Domain.Core.Communication;

public class BaseResponse
{
    public bool Success { get; set; }
    public ResponseErrorMessages Errors { get; set; } = new();
}
public class ResponseErrorMessages
{
    public List<string> Mensagens { get; set; } = new();
}
