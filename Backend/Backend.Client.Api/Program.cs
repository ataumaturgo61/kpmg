using AutoMapper;
using Backend.Application.Base;
using Backend.Application.Person.GetAll;
using Backend.Domain.Core.Repositories;
using Backend.Domain.Repositories;
using Backend.Domain.Services;
using Backend.Infra;
using Backend.Infra.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Globalization;
using Backend.Application.Person.GetById;
using Backend.Application.Person.GetCarsByPerson;
using Backend.Client.Api.Middlewares;

var builder = WebApplication.CreateBuilder(args);


builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddMediatR(x => x.RegisterServicesFromAssembly(typeof(Command).Assembly));

builder.Services.AddDbContext<BackendContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
builder.Services.AddScoped<IPersonRepository, PersonRepository>();
builder.Services.AddScoped<IPersonService, PersonService>();

var config = new MapperConfiguration(c => {
    c.AddProfile<GetAllProfile>(); 
    c.AddProfile<GetByIdQueryProfile>(); 
    c.AddProfile<GetCarsByPersonProfile>(); 
});

builder.Services.AddSingleton<IMapper>(s => config.CreateMapper());

CultureInfo.CurrentCulture = CultureInfo.InvariantCulture;

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.UseMiddleware<ExceptionMiddleware>();

app.UseCors(_ => _.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());

app.MapControllers();

app.Run();
