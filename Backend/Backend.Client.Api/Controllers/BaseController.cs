﻿using Backend.Application.Base;
using FluentValidation.Results;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Client.Api.Controllers;

[ApiController]
[Route("api/v1/[controller]")]
public abstract class BaseController : ControllerBase
{
    protected readonly IMediator _mediator;
    protected ICollection<string> Errors = new List<string>();

    protected BaseController(IMediator mediator)
    {
        _mediator = mediator;
    }

    protected async Task<IActionResult> SendAsync<T>(T request) where T : Command
    {
        var result = await _mediator.Send(request);

        return CustomResponse(result);
    }

    protected async Task<IActionResult> QueryAsync<T>(T query)
    {
        var result = await _mediator.Send(query);

        return CustomResponse(result);
    }

    protected IActionResult CustomResponse(object? result = null)
    {
        if (IsValid())
        {
            return Ok(result);
        }

        return BadRequest(new ValidationProblemDetails(new Dictionary<string, string[]>
        {
            { "Mensagens", Errors.ToArray() }
        }));
    }
     
    protected IActionResult CustomResponse(ValidationResult validationResult)
    {
        foreach (var erro in validationResult.Errors)
        {
            AddError(erro.ErrorMessage);
        }

        return CustomResponse();
    }
 
    protected bool ResponseHasErrors(ValidationResult response)
    {
        if (response is null || !response.Errors.Any()) return false;

        foreach (var message in response.Errors)
            AddError(message.ErrorMessage);

        return true;
    }

    protected bool IsValid()
        => !Errors.Any();

    protected void AddError(string erro)
        => Errors.Add(erro);
     
}