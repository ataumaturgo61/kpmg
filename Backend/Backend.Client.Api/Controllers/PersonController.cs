﻿using Backend.Application.Person.AddCar;
using Backend.Application.Person.CreatePerson;
using Backend.Application.Person.GetAll;
using Backend.Application.Person.GetById;
using Backend.Application.Person.GetCarsByPerson;
using Backend.Application.Person.RemovePerson;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Client.Api.Controllers;

public class PersonController : BaseController
{
    public PersonController(IMediator mediator) : base(mediator) { }

    [HttpPost]
    public async Task<IActionResult> Create([FromBody] CreatePersonCommand command)
    {
        return await SendAsync(command);
    }

    [HttpGet]
    public async Task<IActionResult> GetAll()
    {
        return await QueryAsync (new GetAllQuery());
    }

    [HttpPost("add-car")]
    public async Task<IActionResult> AddCar([FromBody] AddCarCommand command)
    {
        return await SendAsync(command);
    }

    [HttpGet("cars")]
    public async Task<IActionResult> GetCarsByPerson([FromQuery] Guid personId)
    {
        var query = new GetCarsByPersonQuery
        {
            PersonId = personId
        };

        return await QueryAsync(query);
    }

    [HttpDelete]
    public async Task<IActionResult> RemovePerson([FromQuery] RemovePersonCommand command)
    {
        return await SendAsync(command);
    }

    [HttpGet("get-by-id/{personId:guid}")]
    public async Task<IActionResult> GetById([FromRoute] Guid personId)
    {
        var query = new GetByIdQuery(personId);
        return await QueryAsync(query);
    }
}