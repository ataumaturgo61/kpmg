﻿namespace Backend.Domain.Services;

public interface IPersonService
{
    bool IsValidCpf(string cpf);
}