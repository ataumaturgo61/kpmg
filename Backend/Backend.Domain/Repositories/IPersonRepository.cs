﻿using Backend.Domain.Core.Repositories;
using Backend.Domain.Entities;

namespace Backend.Domain.Repositories;

public interface IPersonRepository : IRepository<Person>
{
    Task<bool> CpfExists(string cpf);
    Task Add(Person person);
    void Delete(Person person);
    void Update(Person person);
    Task<Person> FindById(Guid id);
    Task<IEnumerable<Person>> GetAll();
    Task AddCars(IEnumerable<Car> cars);
    Task AddCar(Car car);
    void RemoveCar(Car car);
    void RemoveCars(IEnumerable<Car> cars);
    Task<Person> GetById(Guid personId);
    Task<IEnumerable<Car>> GetCars(List<Guid> carsIds);
    Task<IEnumerable<Car>> GetCars(Guid personId);
}