﻿using Backend.Domain.Core.Entities;

namespace Backend.Domain.Entities;

public class Document : ValueObject
{
    public string Value { get; private set; }

    public Document(string value)
    {
        Value = value;
    }
}