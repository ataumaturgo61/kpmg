﻿using Backend.Domain.Core.Entities;
using Backend.Domain.Exceptions;

namespace Backend.Domain.Entities;

public class Person : Entity, IAggregateRoot
{
    private List<Car> _cars;

    public Document Cpf { get; private set; }
    public string Name { get; private set; }
    public IReadOnlyCollection<Car> Cars => _cars;

    public Person(string name, string cpf)
    {
        Name = name;
        Cpf = new Document(cpf);

        _cars = new List<Car>();
    }

    protected Person() { }

    public void ChangeName(string name)
    {
        if (name == Name) return;

        if (string.IsNullOrEmpty(name))
        {
            throw new BusinessRuleException(nameof(name));
        }

        Name = name;
    }

    public void ChangeCpf(string cpf)
    {
        if (cpf == Cpf.Value) return;

        if (string.IsNullOrWhiteSpace(cpf))
            throw new BusinessRuleException("CPF inválido");

        Cpf = new(cpf);
    }
}