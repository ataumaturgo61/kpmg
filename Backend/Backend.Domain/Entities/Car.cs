﻿using Backend.Domain.Core.Entities;

namespace Backend.Domain.Entities;

public class Car : Entity
{
    public Document Renavam { get; private set; }

    public string LicensePlate { get; private set; }

    public Guid PersonId { get; private set; }

    public Person Person { get; private set; }

    public Car(string renavam, string licensePlate, Guid personId)
    {
        Renavam = new Document(renavam);
        LicensePlate = licensePlate;
        PersonId = personId;
    }

    protected Car() { }
}