export interface getPersonQueryResult{
    id: string;
    name: string;
    cpf: string;
}

export interface getPersonByIdQueryResult{
    id: string;
    name: string;
    cpf: string;
    totalCars: number;
}