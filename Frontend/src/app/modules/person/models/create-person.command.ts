export interface CreatePersonCommand {
    name: string;
    cpf: string;
}

export interface RemovePersonCommand{
    personId: string;
}

export interface CreateCarCommand{
    renavam: string;
    licensePlate: string;
    personId: string;
}