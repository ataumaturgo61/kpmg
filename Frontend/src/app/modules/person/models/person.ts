export interface Person {
    id: string;
    cpf: Document;
    name: string;
    cars: Car[];
}

export interface Car {
    id: string;
    renavam: string;
    licensePlate: string;
    personId: string;
}