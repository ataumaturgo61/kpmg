export interface GetCarsResultQuery{
    renavam: string;
    licensePlate: string;
}