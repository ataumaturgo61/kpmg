import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CreatePersonCommand } from '../models/create-person.command';
import { PersonService } from '../person.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrl: './create.component.scss'
})
export class CreateComponent {

  form: FormGroup;

  constructor(private fb: FormBuilder, private service: PersonService) {
    this.form = this.fb.group({
      name: ['', Validators.required],
      cpf: ['', [Validators.required]]
    });
  }

  onSubmit(): void {
    if (this.form.valid) {
      let createPersonCommand: CreatePersonCommand = {
        name: this.form.get('name')?.value,
        cpf: this.form.get('cpf')?.value
      }

      this.service.createPerson(createPersonCommand).subscribe(res => {
        window.location.reload();
      });
    }
  }
}
