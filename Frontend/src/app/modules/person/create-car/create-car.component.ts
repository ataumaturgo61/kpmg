import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CreateCarCommand, CreatePersonCommand } from '../models/create-person.command';
import { PersonService } from '../person.service';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-create-car',
  templateUrl: './create-car.component.html',
  styleUrl: './create-car.component.scss'
})
export class CreateCarComponent {

  form: FormGroup;

  constructor(private fb: FormBuilder, private service: PersonService, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.form = this.fb.group({
      renavam: ['', Validators.required],
      placa: ['', [Validators.required]]
    });
  }

  onSubmit(): void {
    if (this.form.valid) {
      let createCarCommand: CreateCarCommand = {
        renavam: this.form.get('renavam')?.value,
        licensePlate: this.form.get('placa')?.value,
        personId: this.data.personId
      }

      this.service.addCarToPerson(createCarCommand).subscribe(res => {
        window.location.reload();
      });
    }
  }
}
