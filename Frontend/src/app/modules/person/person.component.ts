import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CreateComponent } from './create/create.component';
import { PersonService } from './person.service';
import { RemovePersonCommand } from './models/create-person.command';
import { CarsComponent } from './cars/cars.component';
import { CreateCarComponent } from './create-car/create-car.component';


@Component({
    selector: 'person',
    templateUrl: `./person.component.html`,
    styleUrls: ['./person.component.scss']
})
export class PersonComponent implements OnInit {
    displayedColumns: string[] = ['actions', 'name', 'cpf', 'cars'];
    public dataSource: any;

    constructor(public dialog: MatDialog, private service: PersonService) { }

    ngOnInit(): void {
        this.service.getAllPersons().subscribe((res) => {
            this.dataSource = res;
        })
    }

    openDialog() {
        this.dialog.open(CreateComponent, {
            width: '300px',
            height: '300px'
        });
    }

    openDialogCars(personId: string) {
        this.dialog.open(CarsComponent, {
            width: '500px',
            height: '500px',
            data: { personId: personId }
        });
    }

    openDialogAddCars(personId: string) {
        this.dialog.open(CreateCarComponent, {
            width: '300px',
            height: '300px',
            data: { personId: personId }
        });
    }


    delete(id: string) {
        let removePersonCommand: RemovePersonCommand = {
            personId: id
        };

        this.service.removePerson(removePersonCommand)
            .subscribe(_ => {
                this.service.getAllPersons().subscribe((res) => {
                    this.dataSource = res;
                })
            })
    }

}
