import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonComponent } from './person.component';
import { ReactiveFormsModule } from '@angular/forms';
import { PersonRoutingModule } from './person-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { PersonService } from './person.service';
import { CreateComponent } from './create/create.component';
import { CarsComponent } from './cars/cars.component';
import { CreateCarComponent } from './create-car/create-car.component';

@NgModule({
  declarations: [PersonComponent, CreateComponent, CarsComponent, CreateCarComponent],
  providers: [PersonService],
  imports: [
    CommonModule,
    PersonRoutingModule,
    ReactiveFormsModule,
    SharedModule,
    HttpClientModule,
  ]
})
export class PersonModule { }
