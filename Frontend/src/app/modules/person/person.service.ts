import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { CreateCarCommand, CreatePersonCommand, RemovePersonCommand } from './models/create-person.command';
import { getPersonQueryResult } from './models/get-person-result.query';
import { GetCarsResultQuery } from './models/get-cars-result.query';

@Injectable({
  providedIn: 'root'
})
export class PersonService {
  private baseUrl = 'https://localhost:7003/api/v1/person';
  horizontalPosition: MatSnackBarHorizontalPosition = 'start';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  constructor(private http: HttpClient, private _snackBar: MatSnackBar) { }

  createPerson(command: CreatePersonCommand): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}`, command).pipe(
      catchError(error => {
        this.handleError(error);
        return throwError(error);
      })
    );
  }

  getAllPersons(): Observable<Array<getPersonQueryResult>> {
    return this.http.get<Array<getPersonQueryResult>>(`${this.baseUrl}`).pipe(
      catchError(error => {
        this.handleError(error);
        return throwError(error);
      })
    );
  }

  addCarToPerson(command: CreateCarCommand): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/add-car`, command).pipe(
      catchError(error => {
        this.handleError(error);
        return throwError(error);
      })
    );
  }

  getCarsByPerson(personId: string): Observable<Array<GetCarsResultQuery>> {
    return this.http.get<any>(`${this.baseUrl}/cars?personId=${personId}`).pipe(
      catchError(error => {
        this.handleError(error);
        return throwError(error);
      })
    );
  }

  removePerson(command: RemovePersonCommand): Observable<any> {
    return this.http.delete<any>(`${this.baseUrl}?personId=${command.personId}`).pipe(
      catchError(error => {
        this.handleError(error);
        return throwError(error);
      })
    );
  }

  getPersonById(personId: string): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/get-by-id/${personId}`).pipe(
      catchError(error => {
        this.handleError(error);
        return throwError(error);
      })
    );
  }

  private handleError(error: any): void {
    console.error('Erro:', error);
    let errorMessage = 'Algo de errado aconteceu.';

    if (error instanceof HttpErrorResponse && error.status === 400 && error.error && error.error.errors && error.error.errors.Mensagens) {
      const mensagens = error.error.errors.Mensagens;
      if (Array.isArray(mensagens) && mensagens.length > 0) {
        errorMessage = mensagens[0];
      }
    }

    this.openSnackBar(errorMessage);
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, 'Close', {
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });
  }
}