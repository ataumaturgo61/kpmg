import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { PersonService } from '../person.service';
@Component({
    selector: 'cars',
    templateUrl: `./cars.component.html`,
    styleUrls: ['./cars.component.scss']
})
export class CarsComponent implements OnInit {
    displayedColumns: string[] = ['renavam', 'licensePlate'];
    public dataSource: any;

    constructor(public dialog: MatDialog, private service: PersonService, @Inject(MAT_DIALOG_DATA) public data: any) { }

    ngOnInit(): void {
        console.log('aqui')
        this.service.getCarsByPerson(this.data.personId).subscribe((res) => {
            this.dataSource = res;
            console.log(this.dataSource)
        })
    }


}
