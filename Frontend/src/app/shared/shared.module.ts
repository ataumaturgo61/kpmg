import { NgModule } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatListModule } from '@angular/material/list';

@NgModule({
    declarations: [],
    imports: [MatTableModule, MatButtonModule, MatDialogModule, MatInputModule, MatSnackBarModule, MatListModule],
    providers: [],
    bootstrap: [],
    exports: [MatTableModule, MatButtonModule, MatDialogModule, MatInputModule, MatSnackBarModule, MatListModule]
})
export class SharedModule { }