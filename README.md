# kpmg

Teste para KPMG

## Getting started
 
 Para rodar o projeto vocês precisam configurar o banco de dados na máquina de vocês, no meu PC eu subi um container docker

 docker pull mcr.microsoft.com/mssql/server

 docker run --name sqlserver -e "ACCEPT_EULA=Y" -e "MSSQL_SA_PASSWORD=Suf68421!" -p 1433:1433 -d mcr.microsoft.com/mssql/server

 usem esses comando que vocês teram o container, se tiverem o Docker instalado, se não podem usar o Sql Server Express, normal, só que precisaram alterar a Connection String
 dentro do arquivo AppSettings.Json

 após o banco configurado, vocês precisarão subir o banco de dados

 pra isso você precisará usar o comando Update-Database dentro do CLI do nuget no visual studio 

 Update-Database

 ou você pode fazer por linha de comando

 você pode entrar no projeto Infrastructure: Backend.Infra

e rodar o seguinte comando:

dotnet ef database update --startup-project ../Backend.Client.Api 

após isso, para rodar o frontend você precisa usar o comando:

npm install

e depois para rodar

 ng s -o
